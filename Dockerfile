FROM python:3.11.8
ENV FLASK_APP run.py
WORKDIR /application
COPY src /application
RUN pip install -r requirements.txt
EXPOSE 5004
CMD [ "python", "run.py"]