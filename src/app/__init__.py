from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

def run_app(port_number, debug_status, host_addr):
    app.run(port=port_number, debug=debug_status, host=host_addr)