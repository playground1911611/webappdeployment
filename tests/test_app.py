import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'src')))

from app import app

def test_app():
  
  response = app.test_client().get('/')
  assert response.status == '200 OK'
  assert response.data == b'Hello, World!'