# Simple Flask app deployment using GitLab CI/CD

This repository contains simple web application, which the only purpose is to display "Hello, World!". <br />
The task is focused on creating a sample pipeline that illustrates how such applications can be built, tested and deployed in the cloud environment.


![General_overview](/imgs/general_overview.png)

The image above illustrates how the developed system works and which technologies are used. <br />
At the beginning it can be noticed that proposed stages are diffrent than in typical example (build -> test -> run). The following description will explain this.

## 1. Test
In this stage, a container is built based on the Docker image containing the required version of python, in which all the required libraries are installed (their list can be found in 'src/requirements.txt').
Then, using 'pytest', all implemented tests are run (they are located in the 'tests' directory in 'test_app.py' file). In fact, a test client is created and we only check that the content displayed on the page is as expected. <br />
Additionally in this stage 'SAST' and 'Secrets Detection' are integrated for the security testing.<br />
<br />
There is no 'Build' stage before testing, because we handle here with really simply app, which does not require it before.

## 2. Build 
During this stage we login to Docker Hub using predefined GitLab variables, which store password and username. <br />
Using 'Docker in Docker' concept we build Docker image of our application (based on Dockerfile available in repository) and then we push it to Docker Hub. <br />
Image name and image tag are also stored in predefined GitLab variables. 

* If pipeline is run from 'main' branch then image is pushed to registry with 'release tag', if from 'develop' than with standard one, and from feature branch this stage is not executed. (I assumed that we work using Gitflow workflow)

## 3. Scan
This stage is added because by default "Container Scanning" is connected with "Test" stage. In this case due to the decision to start this pipeline with 'Test' stage it was needed to add additional stage to ensure that Container Scanning would be always using the newest app image. 

## 4. Deploy
This stage is run only when the changes are made to main branch. In this case, the necessary predefined GitLab variables are required: Google Cloud Project ID and generated key, which allows to use newly created service account on Google Cloud Platform. <br />
Not only do we authenticate, but we also submit a build job to Google Cloud Build and then deploy our application to Cloud Run. Configuration file 'cloudbuild.yaml' is also available in repository. We use here docker image with installed Google Cloud SDK. <br />

The application is currently available here: (https://cloudrunservice-sckvhoklfq-uc.a.run.app/). <br />
Docker image of this simple web app is also publicly available: (https://hub.docker.com/repository/docker/olafzdziebko/flask_hello_world/general).

* For the proper functioning of the system, the google cloud service account must be assigned the following roles: Cloud Build Editor
Cloud Build Service Agent and Cloud Run Admin. It can be done in 'IAM & Admin' settings.

## Project Tree
Here project tree with the most important folders and files can be seen: <br />

![Project_Tree](/imgs/project_tree.png)

## Required GitLab variables
Here are all the mentioned variables, which have to be set in GitLab:
![Variables](/imgs/variables.png)

## Important
* Taking into consideration security scanning only SAST, Secret Detection and Container Scanning are used, because another possibilities like DAST are available only for Ultimate GitLab users.
* Pipeline is triggered by each commit and merge. It works as it was said, the range depends on the branch. It can also be run manually on GitLab website.

## View of the successful execution of the pipeline
![Pipeline](/imgs/pipeline_view.png)
